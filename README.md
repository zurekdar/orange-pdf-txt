## itextsharp

extract pdf form to txt

- itextsharp.dll: the core library
- itextsharp.xtra.dll: extra functionality (PDF 2!)
- itextsharp.pdfa.dll: PDF/A-related functionality
You can find the latest release here:
http://sourceforge.net/projects/itextsharp/files/itextsharp/

iTextSharp is licensed as AGPL software.

---
