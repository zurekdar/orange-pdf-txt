﻿
Imports System.Text
Imports iTextSharp.text.pdf.parser

Public Class TableExtractionStrategy
    Implements ITextExtractionStrategy

    Structure _structure
        Dim myTxt As String
        Dim myX As Int64
        Dim myY As Int64
    End Structure

    Public Chunks As New List(Of Chunk)()

    Public Overridable Sub BeginTextBlock() Implements ITextExtractionStrategy.BeginTextBlock
    End Sub
    Public Overridable Sub EndTextBlock() Implements ITextExtractionStrategy.EndTextBlock
    End Sub
    Public Overridable Sub RenderImage(ByVal renderInfo As iTextSharp.text.pdf.parser.ImageRenderInfo) Implements ITextExtractionStrategy.RenderImage
    End Sub

    Public Function GetResultantText() As String Implements ITextExtractionStrategy.GetResultantText
        Dim text = New StringBuilder()
        Dim result As New List(Of _structure)

        Chunks.Sort()

        Dim prevChunk As Chunk = Nothing

        For Each chunk As TableExtractionStrategy.Chunk In Chunks
            Dim _string As String = chunk.Text

            Dim _start As Vector = chunk.TopLeft
            Dim _end As Vector = chunk.BottomRight
            Dim _X As Int64 = _start(0)
            Dim _Y As Int64 = _start(1)
            Dim _space As String = Replace(Space(_X / 10), " ", ".")

            _string = Encoding.UTF8.GetString(Encoding.Convert(Encoding.Default, Encoding.UTF8, Encoding.Default.GetBytes(_string)))

            If IsNothing(prevChunk) Then
                _string = "|" + _string
            Else
                If chunk.Text.Length > 0 AndAlso chunk.Text(0) <> " " Then
                    Dim spacing As Single = prevChunk.BottomRight.Subtract(_start).Length

                    If spacing > chunk.RenderInfo.GetSingleSpaceWidth() / 2 Then
                        _string = "|" + _string
                    End If
                End If
            End If

            'y
            'y
            'y
            ' x x x

            Dim _result As New _structure
            _result.myX = _X
            _result.myY = _Y
            _result.myTxt = _string
            result.Add(_result)

            ' -----------------------------------------------------------------------------
            ' ORG
            ' -----------------------------------------------------------------------------
            If prevChunk Is Nothing AndAlso String.IsNullOrWhiteSpace(_string) Then
                Continue For
            End If

            If prevChunk IsNot Nothing AndAlso Not chunk.SameLine(prevChunk, 6) Then
                text.Append(vbLf & vbLf)
            End If

            text.Append(_string)

            prevChunk = chunk
        Next

        Dim _text As String = TableText(result)

        Return _text
    End Function

    Private Function StripSpaces(input As String) As String
        Return String.Join(" ", input.Split(New Char() {}, StringSplitOptions.RemoveEmptyEntries))
    End Function

    Public Sub RenderText(renderInfo As TextRenderInfo) Implements ITextExtractionStrategy.RenderText
        Dim _text As String = renderInfo.GetText()
        Dim _segment As LineSegment = renderInfo.GetBaseline()
        Dim _start As Vector = _segment.GetStartPoint()
        Dim _end As Vector = _segment.GetEndPoint()

        Chunks.Add(New Chunk() With {
            .TopLeft = _start,
            .BottomRight = _end,
            .Text = _text,
            .RenderInfo = renderInfo
        })
    End Sub

    Private Function TableText(result As List(Of _structure)) As String
        Dim _dr As Data.DataRow = Nothing
        Dim _dt As New Data.DataTable
        Dim _dc As New Data.DataTable

        _dt.Columns.Add("Row", System.Type.GetType("System.Int64"))
        _dt.Columns.Add("Col", System.Type.GetType("System.Int64"))
        _dt.Columns.Add("Slr", System.Type.GetType("System.Int64"))
        _dt.Columns.Add("Pag", System.Type.GetType("System.Int64"))
        _dt.Columns.Add("Txt", System.Type.GetType("System.String"))

        _dc.Columns.Add("Row", System.Type.GetType("System.Int64"))
        _dc.Columns.Add("Col", System.Type.GetType("System.Int64"))
        _dc.Columns.Add("Slr", System.Type.GetType("System.Int64"))
        _dc.Columns.Add("Pag", System.Type.GetType("System.Int64"))
        _dc.Columns.Add("Txt", System.Type.GetType("System.String"))

        For Each chunk As _structure In result
            Dim _nword As String = chunk.myTxt
            Dim _rectY As String = Convert.ToInt64(chunk.myY).ToString ' row
            Dim _rectX As String = Convert.ToInt64(chunk.myX).ToString ' col
            Dim _myslr As Int64 = 0 ' lewa, prawa
            Dim _nrtmp As String = _rectY + "," + _rectX

            _dr = _dc.NewRow() : _dr("Row") = _rectY : _dr("Col") = _rectX : _dr("Slr") = _myslr : _dr("Pag") = 0 : _dr("Txt") = _nword : _dc.Rows.Add(_dr)
        Next

        ' TOLERANCJA
        ' -----------------------------------------------------------
        Dim _nrow As Int64 = 0
        Dim _temp As String = String.Empty

        _dc.DefaultView.Sort = "Row DESC, Col ASC"
        For Each _dr In _dc.DefaultView.ToTable.Rows
            Dim _txt As String = _dr("Txt").ToString
            Dim _row As Int64 = _dr("Row")
            Dim _col As Int64 = _dr("Col")
            Dim _slr As Int64 = _dr("Slr")
            If _nrow = 0 Then
                _nrow = _row
            End If
            If _nrow > (_row + 6) Then
                _nrow = _row
            End If
            Dim _str As String = _nrow.ToString + "," + _col.ToString '+ "," + _slr.ToString
            _temp += _str + " |" + _txt + vbCrLf
            _dr = _dt.NewRow() : _dr("Row") = _nrow : _dr("Col") = _col : _dr("Slr") = _slr : _dr("Pag") = 0 : _dr("Txt") = _txt : _dt.Rows.Add(_dr)
        Next

        ' CAŁY TEKST
        ' -----------------------------------------------------------
        Dim _text As String = String.Empty
        Dim _tmp As Int64 = 0
        _dt.DefaultView.Sort = "Row DESC, Col ASC"
        For Each _dr In _dt.DefaultView.ToTable.Rows
            Dim _txt As String = _dr("Txt").ToString
            Dim _row As Int64 = _dr("Row")
            Dim _col As Int64 = _dr("Col")
            Dim _slr As Int64 = _dr("Slr")
            Dim _str As String = _col.ToString

            _txt = _txt.Replace(vbCr, "").Replace(vbLf, "")

            If _tmp = _row Then
                _text += _txt
            Else
                _text += vbCrLf + _str + _txt
            End If
            _tmp = _row
        Next

        Return _text
    End Function

    Public Class Chunk
        Implements IComparable(Of Chunk)

        Private m_TopLeft As Vector
        Private m_BottomRight As Vector
        Private m_Text As String
        Private m_RenderInfo As TextRenderInfo

        Public Property RenderInfo() As TextRenderInfo
            Get
                Return m_RenderInfo
            End Get
            Set
                m_RenderInfo = Value
            End Set
        End Property

        Public Property TopLeft() As Vector
            Get
                Return m_TopLeft
            End Get
            Set
                m_TopLeft = Value
            End Set
        End Property

        Public Property BottomRight() As Vector
            Get
                Return m_BottomRight
            End Get
            Set
                m_BottomRight = Value
            End Set
        End Property

        Public Property Text() As String
            Get
                Return m_Text
            End Get
            Set
                m_Text = Value
            End Set
        End Property

        Public Function CompareTo(other As Chunk) As Integer Implements IComparable(Of Chunk).CompareTo
            Dim y1 = CInt(Math.Round(TopLeft(1)))
            Dim y2 = CInt(Math.Round(other.TopLeft(1)))

            If y1 < y2 Then
                Return 1
            End If

            If y1 > y2 Then
                Return -1
            End If

            Dim x1 = CInt(Math.Round(TopLeft(0)))
            Dim x2 = CInt(Math.Round(other.TopLeft(0)))

            If x1 < x2 Then
                Return -1
            End If

            If x1 > x2 Then
                Return 1
            End If

            Return 0
        End Function

        Public Function SameLine(other As Chunk, Optional maxDiff As Integer = 0) As Boolean
            Dim diff = Math.Abs(TopLeft(1) - other.TopLeft(1))

            Return diff <= maxDiff
        End Function
    End Class
End Class