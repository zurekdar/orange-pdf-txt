﻿Imports System.IO
Imports iTextSharp.text
Imports iTextSharp.text.pdf.parser
Imports System.Text.RegularExpressions
Imports System.Data.SqlClient

Module mod_Main
    ' Path
    Private exe_Path As String = Reflection.Assembly.GetExecutingAssembly.Location
    Private app_Path As String = exe_Path.Substring(0, exe_Path.LastIndexOf("\"))
    Private ini_Path As String = app_Path & "\app_PdfToTxt.ini"
    Private ini_Read As New cls_Utils("12345678")

    ' Sql
    Private sql_DataSource As String = ini_Read.ReadIni(ini_Path, "Connection SQL", "ServerSQL")
    Private sql_InitialCatalog As String = ini_Read.ReadIni(ini_Path, "Connection SQL", "InitialCatalog")
    Private sql_UserID As String = ini_Read.DecryptUrl(ini_Read.ReadIni(ini_Path, "Connection SQL", "UserSQL"))
    Private sql_UserPassword As String = ini_Read.DecryptUrl(ini_Read.ReadIni(ini_Path, "Connection SQL", "UserPass"))

    ' Eml
    Private eml_DataSource As String = ini_Read.ReadIni(ini_Path, "Connection EML", "ServerSQL")
    Private eml_InitialCatalog As String = ini_Read.ReadIni(ini_Path, "Connection EML", "InitialCatalog")
    Private eml_UserID As String = ini_Read.DecryptUrl(ini_Read.ReadIni(ini_Path, "Connection EML", "UserSQL"))
    Private eml_UserPassword As String = ini_Read.DecryptUrl(ini_Read.ReadIni(ini_Path, "Connection EML", "UserPass"))

    ' Connection SQL
    Private _connectionSql As New SqlConnectionStringBuilder(String.Format("Data Source={0};Initial Catalog={1};Persist Security Info=True;User ID={2};Password={3}" _
                                                        , sql_DataSource _
                                                        , sql_InitialCatalog _
                                                        , sql_UserID _
                                                        , sql_UserPassword)) With {.MultipleActiveResultSets = True, .AsynchronousProcessing = True}

    ' Connection Eml
    Private _connectionEml As New SqlConnectionStringBuilder(String.Format("Data Source={0};Initial Catalog={1};Persist Security Info=True;User ID={2};Password={3}" _
                                                        , eml_DataSource _
                                                        , eml_InitialCatalog _
                                                        , eml_UserID _
                                                        , eml_UserPassword)) With {.MultipleActiveResultSets = True, .AsynchronousProcessing = True}

    Sub Main()

        Dim _dt As New DataTable
        Dim _da As New SqlDataAdapter

        Using connection As New SqlConnection(_connectionSql.ConnectionString)
            connection.Open()
            '
            Using command As New SqlCommand
                command.Connection = connection
                command.CommandType = CommandType.StoredProcedure
                command.CommandTimeout = 0
                '
                command.CommandText = "_select_pdf"
                command.Parameters.Clear()

                _da.SelectCommand = command
                _da.Fill(_dt)
            End Using
            '
            For Each _row As DataRow In _dt.Rows
                Dim _id As String = _row("Id").ToString
                Dim _path As String = _row("file_FullPath").ToString

                Dim _fi As New FileInfo(_path)
                Dim _fs As New FileStream(_fi.FullName, FileMode.Open, FileAccess.Read)
                Dim _rb As New BinaryReader(_fs)
                Dim _byte As Byte() = _rb.ReadBytes(_rb.BaseStream.Length) : _fs.Close()
                Dim _reader As iTextSharp.text.pdf.PdfReader = Nothing

                Try
                    _reader = New iTextSharp.text.pdf.PdfReader(_byte)
                Catch ex As iTextSharp.text.exceptions.InvalidPdfException
                    Dim _ex As String = "Plik: " + _fi.Name + " " + ex.Message
                    SendError(_ex)
                    Continue For
                End Try

                Dim _size As iTextSharp.text.Rectangle = _reader.GetPageSize(1)
                Dim _rectangle As New iTextSharp.text.Rectangle(0, 0, _size.Width, _size.Height)
                Dim _filter As New RegionTextRenderFilter(_rectangle)
                Dim _text As String = String.Empty

                For _number As Int32 = 1 To _reader.NumberOfPages
                    Dim _strategy As New FilteredTextRenderListener(New TableExtractionStrategy, _filter)

                    _text += "----------------------------- page: " + _number.ToString + vbCrLf
                    _text += PdfTextExtractor.GetTextFromPage(_reader, _number, _strategy) + vbCrLf + vbCrLf
                Next

                ' Usuwa białe spacje z pominięciem Lf Cr
                _text = Regex.Replace(_text, "[^\S\n\r]{2,}", " ", RegexOptions.IgnoreCase)
                '
                Using command As New SqlCommand
                    command.Connection = connection
                    command.CommandType = CommandType.StoredProcedure
                    command.CommandTimeout = 0
                    '
                    command.CommandText = "_update_pdf"
                    command.Parameters.Clear()
                    command.Parameters.AddWithValue("@Id", _id)
                    command.Parameters.AddWithValue("@file_Text", _text)
                    command.ExecuteNonQuery()
                End Using
            Next

        End Using
    End Sub

    Private Sub SendError(_info As String)
        Try
            Console.WriteLine("{0}", _info)
            Using connection As New SqlConnection(_connectionEml.ConnectionString)
                connection.Open()
                '
                Dim _subject As String = "[Errorr] Parse pdf to txt"
                Dim _message As String = _info
                Dim _addressto As String = String.Empty
                Dim _addressbcc As String = String.Empty
                Dim _system As String = "PDFTOTXT"
                Dim _status As String = 0
                '
                Using command As New SqlCommand()
                    command.Connection = connection
                    command.CommandTimeout = 0
                    '
                    command.CommandText = "INSERT INTO Smtp (Subject, Message, AddressTo, AddressBcc, System, Status) VALUES (@Subject, @Message, @AddressTo, @AddressBcc, @System, @Status)"
                    command.Parameters.Clear()
                    command.Parameters.AddWithValue("@Subject", _subject)
                    command.Parameters.AddWithValue("@Message", _message)
                    command.Parameters.AddWithValue("@AddressTo", _addressto)
                    command.Parameters.AddWithValue("@AddressBcc", _addressbcc)
                    command.Parameters.AddWithValue("@System", _system)
                    command.Parameters.AddWithValue("@Status", _status)
                    command.ExecuteNonQuery()
                End Using
            End Using
        Catch ex_sql As Exception
        End Try
    End Sub

End Module
